package com.pranish.fingerprintauth;

import android.Manifest;
import android.app.KeyguardManager;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

/***
 * A sample project which implements finger print authentication.
 * The process :-
 * 1. Verify that the lock screen is secure, or in other words, it is protected by PIN, password or pattern
 2. Verify that at least one fingerprint is registered on the smartphone
 3. Get access to Android keystore to store the key used to encrypt/decrypt an object
 4. Generate an encryption key and the Cipher
 5. Start the authentication process
 6. Implement a callback class to handle authentication events
 Reference :- https://www.survivingwithandroid.com/2016/12/android-fingerprint-app-authentication-tutorial.html
 */
public class MainActivity extends AppCompatActivity {
    private static final String KEY_NAME = "okbcsqSH04";
    private TextView txtMesssage;


    private KeyStore keyStore;

    private FingerprintManager fingerprintManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FingerprintManager.CryptoObject cryptoObject = null;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtMesssage = (TextView) findViewById(R.id.txtMsg);
        Button btnAuth = (Button) findViewById(R.id.btnAuthenticate);
        final FingerprintHandler fingerprintHandler = new FingerprintHandler(txtMesssage);

        if (!checkFinger()) {
            btnAuth.setEnabled(false);
        } else {
            // Ready to setup the cipher and the key

            try {
                generateKey();
                Cipher cipher = generateCipher();
                cryptoObject = new FingerprintManager.CryptoObject(cipher);
            } catch (Exception e) {
                btnAuth.setEnabled(false);
            }

            final FingerprintManager.CryptoObject finalCryptoObject = cryptoObject;
            btnAuth.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    txtMesssage.setText("Press the finger print sensor");
                    fingerprintHandler.doAuth(fingerprintManager, finalCryptoObject);
                }
            });
        }

    }

    /** The first step is verifying the secure lock screen.
     * This can be done KeyguardManager and FingerprintManager.
     * We get an instance of these two managers using getSystemService:*/

    /**
     * Check if all the security condtions are satisfied
     * Conditions for being true :- 1. Lock screen is secured by pin, password or pattern.
     * 2. Atlease one fingerprint is registered on the smartphone
     */
    private boolean checkFinger() {
        // Keyguard Manager
        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);

        // Fingerprint Manager
        fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            }
            if (!fingerprintManager.isHardwareDetected()) {
                txtMesssage.setText("Fingerprint authentication is not supported in the device");
                return false;
            }

            // Check if atlest one fingerprint is registered or not
            if (!fingerprintManager.hasEnrolledFingerprints()) {
                txtMesssage.setText("No figerprint configured");
                return false;
            }

            // Check if lock screen is protected by a pin, pattern or password
            if (!keyguardManager.isKeyguardSecure()) {
                txtMesssage.setText("Secure lock screen not enabled");
            }

        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Access to android keystore and generate the key to encrypt the data
     */
    private void generateKey() {
        try {
            // Get the reference to the key store
            keyStore = KeyStore.getInstance("AndroidKeyStore");

            // Key generator to genereate the key
            KeyGenerator keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");

            keyStore.load(null);
            keyGenerator.init(new KeyGenParameterSpec.Builder(KEY_NAME, KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7).build());
            keyGenerator.generateKey();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
    }


    /**
     * Once the key is ready, the last step is creating the Android Cipher that uses the key, we have generated before
     */
    private Cipher generateCipher() {
        try {
            Cipher cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/"
                    + KeyProperties.BLOCK_MODE_CBC + "/"
                    + KeyProperties.ENCRYPTION_PADDING_PKCS7);

            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME, null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return cipher;
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | UnrecoverableKeyException | KeyStoreException | InvalidKeyException e) {
            e.printStackTrace();
            return null;
        }
    }

}
