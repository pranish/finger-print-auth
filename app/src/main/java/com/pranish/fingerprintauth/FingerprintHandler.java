package com.pranish.fingerprintauth;

import android.hardware.fingerprint.FingerprintManager;
import android.os.CancellationSignal;
import android.widget.TextView;

/**
 * Created by pranishshrestha on 9/15/17.
 */

public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {
    private TextView tv;

    public FingerprintHandler(TextView tv) {
        this.tv = tv;
    }

    public void doAuth(FingerprintManager manager,
                       FingerprintManager.CryptoObject obj) {
        CancellationSignal signal = new CancellationSignal();

        try {
            manager.authenticate(obj, signal, 0, this, null);
        } catch (SecurityException sce) {
        }
    }

    @Override
    public void onAuthenticationError(int errorCode, CharSequence errString) {
        super.onAuthenticationError(errorCode, errString);
        tv.setText("Authentication error\n" + errString);
    }

    @Override
    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
        super.onAuthenticationHelp(helpCode, helpString);
        tv.setText("Authentication help\n" + helpString);
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        super.onAuthenticationSucceeded(result);
        tv.setText("Authentication Suceeded");
    }

    @Override
    public void onAuthenticationFailed() {
        tv.setText("Authentication failed");
        super.onAuthenticationFailed();
    }


}